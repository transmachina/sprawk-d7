<?php

/**
 * @file
 * Sprawk functionality to send HTML content to the Sprawk server for translation, cleaning and other processing.
 */

define("SPRAWK_LINKMODE_DEFAULT", 'PREFIX_DRUPAL');

/**
 * A check value to ensure that text encoding between Drupal and Sprawk is configured correctly
 */
define("SPRAWK_CHK", utf8_encode("\xE5\xE4\xF6"));

/**
 * Sprawk translation for this node is disabled.
 */
define('SPRAWK_NODE_DISABLED', 0);

/**
 * Sprawk translation for this node is enabled.
 */
define('SPRAWK_NODE_ENABLED', 1);

/**
 * Sprawk translation for this node is the default for its content type.
 */
define('SPRAWK_NODE_DEFAULT', 2);

/**
 * HTTP communication failure: No servers could be reached.
 */
define('SPRAWK_NETWORK_ERROR', 900);

/**
 * HTTP communication failure: Error on Sprawk server.
 */
define('SPRAWK_SERVER_ERROR', 1000);

/**
 * HTTP communication failure: Error on Sprawk server.
 */
define('SPRAWK_CONFIG_PAGE', 'admin/config/regional/sprawk');

/**
 * Implement hook_init - load files necessary for every page
 */
function sprawk_init()
{
  $mpath = drupal_get_path('module', 'sprawk');
  drupal_add_js($mpath . '/sprawk.js');
  drupal_add_css($mpath . '/sprawk.css');

  // On all Sprawk administration pages, check the module configuration and
  // display the corresponding requirements error, if invalid.
  if (empty($_POST) && strpos($_GET['q'], SPRAWK_CONFIG_PAGE) === 0 && user_access('administer sprawk')) {
    // Fetch and display requirements error message, without re-checking.
    module_load_install('sprawk');
    $requirements = sprawk_requirements('runtime', FALSE);
    if (isset($requirements['sprawk']['description'])) {
      drupal_set_message($requirements['sprawk']['description'], 'error');
    }
  }
}

/**
 * Implements hook_hook_info().
 */
function sprawk_hook_info()
{
  $info['sprawk'] = array('group' => 'sprawk');
  return $info;
}

/**
 * Implement hook_menu
 */
function sprawk_menu()
{
  $items[SPRAWK_CONFIG_PAGE] = array(
    'title' => 'Sprawk translation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sprawk_admin_settings'),
    'description' => 'Configure Sprawk Translation settings and preferences',
    'access callback' => 'user_access',
    'access arguments' => array('administer sprawk'),
  );
  $items['sprawk/translate'] = array(
    'callback' => '_sprawk_translate',
    'type' => MENU_CALLBACK,
    'access' => user_access('access sprawk translate'),
  );
  return $items;
}

/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the onthisdate module
 */
function sprawk_permission()
{
  return array(
    'access sprawk translate' => array(
      'title' => t('Access sprawk translate'),
      'description' => t('Use sprawk translations on content'),
    ),
    'administer sprawk' => array(
      'title' => t('Administer sprawk'),
      'description' => t('Administer sprawk translation default settings'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function sprawk_block_info()
{
  include_once DRUPAL_ROOT . '/includes/language.inc';
  $block = array();
  $info = language_types_info();
  foreach (language_types_configurable(FALSE) as $type) {
    $block[$type] = array(
//      'info' => t('Language switcher (Sprawk, @type)', array('@type' => $info[$type]['name'])),
      'info' => t('Language switcher (Sprawk)'),
      // Not worth caching.
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $block;
}

/**
 * Implements hook_block_view().
 *
 * Displays a language switcher. Only show if we have at least two languages.
 */
function sprawk_block_view($type)
{
  if (drupal_multilingual()) {
    $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
    $links = language_negotiation_get_switch_links($type, $path);

    if (isset($links->links)) {
      $class = "language-switcher-sprawk sprawk-notranslate";
      $variables = array('links' => $links->links, 'attributes' => array('class' => array($class)));
      $llist = locale_language_list();
      $enabled_languages = language_list('enabled');
      $enabled_languages = $enabled_languages[1];
//    dpm($enabled_languages,'enabled_languages');
//    dpm($llist);
      $llinks = array();
      foreach ($llist as $langcode => $langname) {
        //       dpm($langcode, 'langcode');
        $langobj = $enabled_languages[$langcode];
//              dpm($langobj, 'langobj');
        $url_options['language'] = $langobj;
        $langnn = $langobj->native;
//      $langnn = t($langobj->name, array(), array('langcode' => $langcode));
        $llink = array('title' => $langnn,
          'href' => $path,
          'language' => $langobj,
          'attributes' =>
          array('hreflang' => $langcode));
//      dpm($llink);
        $llinks[] = $llink;
      }
//    dpm($llinks);
      $content = '<!-- path=' . $path . ',type=' . $type . '-->' . theme('links',
            array('links' => $llinks,
              'attributes' => array('class' => array('sprawk-notranslate'),
                'id' => 'sprawk-translinks')));

      // dpm($content,'content');
      $block['content'] = $content;
      $block['subject'] = t('Languages');
      return $block;
    }
  }
}

/**
 *  This displays the global settings for auto translate. It will create a field for each 3rd party translator included
 *  in the /translators directory of the sprawk module directory. When writing your own include file, you may include
 *  a fieldset for your translator by implementing the sprawk_TRANSLATOR_settings hook (replacing TRANSLATOR with the
 *  name of your .inc file).
 */
function sprawk_admin_settings()
{
  // $r = db_query("SELECT COUNT(*) as total FROM {cache_sprawk}");
  // $data = db_fetch_object($r);

  $form['sprawk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
    '#description' => t("These settings should correspond to an existing Sprawk team registered at the <a href='http://www.sprawk.com'>sprawk.com</a> website. If you don't have a Sprawk team account, <a href='http://www.sprawk.com/en/auth/signupTeam.action'>register now</a> for free."),
    '#collapsible' => FALSE
  );
  $form['sprawk']['sprawk_server'] = array(
    '#type' => 'select',
    '#title' => t('Sprawk server'),
    '#description' => t('The development server has newer features, but can be less stable.') . ' ' .
    t('Contact support@sprawk.com for more details'),
    '#weight' => -9,
    '#options' => array(
      "www.sprawk.com" => t('Production server'),
      "beta.sprawk.com" => t('Testing server'),
      "alpha.sprawk.com" => t('Development server')
    ),
    '#default_value' => variable_get('sprawk_server', 'www.sprawk.com')
  );
  $form['sprawk']['sprawk_groupcode'] = array(
    '#type' => 'textfield',
    '#size' => 16,
    '#title' => t('Team code'),
    '#required' => TRUE,
    '#description' => t('Your team\'s code.'),
    '#weight' => -7,
    '#default_value' => variable_get('sprawk_groupcode', '')
  );
  $form['sprawk']['sprawk_grouppassword'] = array(
    '#type' => 'textfield',
    '#size' => 48,
    '#title' => t('Team password'),
    '#required' => TRUE,
    '#description' => t('Your team\'s password in MD5 format.'),
    '#weight' => -5,
    '#default_value' => variable_get('sprawk_grouppassword', '')
  );
  $form['sprawk']['sprawk_topic'] = array(
    '#type' => 'select',
    '#title' => t('Topic'),
    '#description' => t('The topic to store locale strings.'),
    '#options' => array_merge(array(
      "" => t('Select a topic')), _sprawk_fetch_topics()),
    '#default_value' => variable_get('sprawk_topic', '')
  );
  $form['sprawk']['sprawk_linkmode'] = array(
    '#type' => 'select',
    '#title' => t('Link mode'),
    '#description' => t('Chose how sprawk adjusts links.'),
    '#options' => array(
      'PREFIX_DRUPAL' => 'Add language prefix to internal links',
      //  'UNCHANGED' => 'Leave all links unchanged',
      'PROXY' => 'Use Sprawk proxy service',
    ),
    /*
    '#ajax' => array(
      'callback' => 'sprawk_updateDescLinkmode',
      'wrapper' => 'sprawk_linkmode_desc', // div to be get replce by function output
      'effect' => 'fade'
    ),
    */
    '#default_value' => variable_get('sprawk_linkmode', SPRAWK_LINKMODE_DEFAULT)
  );

  $form['sprawk']['sprawk_linkmode_desc'] = array(
    '#type' => 'markup',
    '#markup' => sprawk_instructions(variable_get('sprawk_linkmode', SPRAWK_LINKMODE_DEFAULT)),
  );

  $form['sprawk']['sprawk_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate debug log'),
    '#description' => t('Record detailed logs to the watchdog'),
    '#default_value' => variable_get('sprawk_debug', 0)
  );
  $form['sprawk']['sprawk_cache_lifetime'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#title' => t('Cache Lifetime'),
    '#description' => t('How many days items should be saved on cache. Default is 30 days'),
    '#default_value' => variable_get('sprawk_cache_lifetime', 30)
  );
  /*
  $form['sprawk']['cache_count'] = array(
    '#type' => 'markup',
    '#value' => format_plural($data->total, 'There is 1 item currently in cache', 'There are @count items currently in cache'),
  );
  */
  $form['sprawk']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Reset cache'),
    '#submit' => array('sprawk_clear_cache_submit'),
  );
  $form['sprawk']['ext_link'] = array(
    '#type' => 'markup',
    '#markup' => '<hr/><p class="sprawk-prompt">' .
    t('For free assistance configuring sprawk with Drupal contact <a href="@mailto">support</a> or visit the <a target="_blank" href="@helpurl">help pages</a>', array('@mailto' => 'mailto:support@sprawk.com', '@helpurl' => 'http://www.sprawk.com/help/')) . '</p>'
  );
  return system_settings_form($form);
}

/**
 * Display help and module information
 * @param $path
 *    section which section of the site we're displaying help
 * @return string help text for section
 */
function sprawk_help($path, $arg)
{
  $output = '';
  switch ($path) {
    case "admin/help#sprawk" :
      $output = '<p>' . t("Displays translations") . '</p>';
      break;
  }
  return $output;
}

function sprawk_filter_info()
{
  $filters['filter_sprawk_translate'] = array(
    'title' => t('Translate content'),
    'description' => t('Translate content using the Sprawk service'),
    'cache' => FALSE,
    //  'prepare callback' => '_sprawk_filter_translate_prepare',
    'process callback' => '_sprawk_filter_translate_process',
  );
  $filters['filter_sprawk_term'] = array(
    'title' => t('Highlight terminology'),
    'description' => t('Highlights key phrases from the text and add a mouseover definition and translation in the user\'s language.'),
    'process callback' => '_sprawk_filter_highlight_process',
  );
  return $filters;
}

/**
 * Look for form descriptions etc. and translate them with Sprawk
 */
function sprawk_form_alter(&$form, &$form_state, $form_id)
{
  // Which types should be processed
  $tr_types = array('#description', '#title', '#options');
  sprawk_form_walker($form, $tr_types);
  // dpm($form, 'altered-form');
}

/**
 * Walker through the form tree and translate the appropriate types
 * @param $array array
 *     the $form array or piece of the form array when called recursivly (must be passed by reference)
 * @param $tr_types array
 *    array of field types to translate
 * @return
 *    All modification are made by reference.
 */
function sprawk_form_walker(&$array, &$tr_types)
{
  foreach ($array as $key => &$value) {
    if (!empty($key) && in_array($key, $tr_types)) {
      $original = $array[$key];
      if (is_array($original)) {
        // translate the values, not the keys
        foreach ($original as $optkey => &$optval) {
          if (is_string($optval)) {
            $translated = strip_tags(ct($optval));
            $original[$optkey] = check_plain($translated);
          }
        }
      } else {
        // it's a string
        $translated = strip_tags(ct($original));
        // dpm($translated, $key);
        $array[$key] = check_plain($translated);
      }
    } elseif (is_array($value)) {
      //call this function again recursively
      sprawk_form_walker($value, $tr_types);
    }
  }
}

/**
 * Sends text to sprawk for translation
 *
 * @param $text
 *    The text in the source language
 * @param $filter
 *  The filter object containing settings for the given format.
 * @param $format
 *  The text format object assigned to the text to be filtered.
 * @param $langcode
 *  The language code of the text to be filtered.
 * @param $cache
 *  A Boolean indicating whether the filtered text is going to be cached in {cache_filter}.
 * @param $cache_id
 *  The ID of the filtered text in {cache_filter}, if $cache is TRUE.
 * @return string The translated text (or the original is the translation failed)
 */
function _sprawk_filter_translate_process($text, $filter, $format, $langcode, $cache, $cache_id)
{
  return _sprawk_filter_translate($text, $langcode);
}

function _sprawk_filter_translate($text, $langcode, $topic = '')
{

  $lang_default = language_default()->language;

  // for the default language there is nothing to do
  if ($langcode == $lang_default) {
    return $text;
  }

  if (!trim($text)) {
    // text was blank;
    return $text;
  }

  // Only add this if not the default language of the site
  $meta_translator = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'Translator',
      'content' => 'Sprawk (http://www.sprawk.com)',
    )
  );
  // Add header meta tag to head
  drupal_add_html_head($meta_translator, 'meta_translator');

  if (variable_get('sprawk_debug', 0)) {
    watchdog('sprawk', "Starting translate (orig=%orig) to %tl", array('%orig' => $text, '%tl' => $langcode));
  }

  $sample = strip_tags($text);
  if (strlen($sample) == 0) {
    if (strlen($text) != 0) {
      watchdog('sprawk', "Skipping blank sample (but orig=%orig)", array('%orig' => $text));
    }
    return $text;
  } elseif (strpos($text, '<?php')) {
    watchdog('sprawk', "Skipping text containing PHP code (orig=%orig)", array('%orig' => $text));
    return $text;
  } elseif (strlen($sample) > 200) {
    $sample = substr($sample, 0, 200);
  }

  $key = 'sprawk:translate_filter:' . md5($text) . $langcode;

  // check the cache first
  // we need to check that the orig data in the cached object is actually the same
  // (there is a small chance that two orig strings with the same md5 crop up from time to time)
  if (($cache = cache_get($key, 'cache_sprawk')) && !empty ($cache->data)) {
    $data = $cache->data;
    if (strcmp($data["orig"], $text) == 0) {
      if (strlen($data["output"]) == 0) {
        watchdog('sprawk', "Cache for key %key contains blank output", array('%key' => $key));
      } else {
        // watchdog('sprawk', "Found translation in cache with key %key", array('%key' => $key));
        return $data["output"];
      }
    } else {
      watchdog('sprawk', "Cache for key %key contains different orig: %orig", array('%key' => $key, '%orig' => $data["text"]));
    }
  } else {
    if (variable_get('sprawk_debug', 0)) {
      watchdog('sprawk', "Key %key not found in cache: %orig", array('%key' => $key, '%orig' => $text), WATCHDOG_DEBUG);
    }
  }

  // prepare the request, add a parameter to prevent
  // recursively attempting to translate
  $uri = '/api/translateHtmlSnippet';

  $linkMode = variable_get('sprawk_linkmode', 'PREFIX_DRUPAL'); // Whether sprawk will adjust links

  // Work out the data
  $params = array(
    "t" => _sprawk_webclean($text),
    "sl" => 'en',
    "tl" => $langcode,
    "topic" => $topic,
    "linkMode" => $linkMode
  );

  $result = _sprawk_doapi($uri, $params);
  $result["orig"] = $text;

  if ($result['status'] == 'success') {

    // Save item to cache
    $days = variable_get('sprawk_cache_lifetime', 30);
    cache_set($key, $result, 'cache_sprawk', time() + ($days * 24 * 60 * 60));
    // log this
    if (variable_get('sprawk_debug', 0)) {
      watchdog('sprawk', "Translation result: %result", array('%result' => $result["output"]), WATCHDOG_DEBUG);
    }
    // return it
    return $result["output"];

  } else {

    // the API call failed
    return $text;
  }

}

/**
 * Send MS Word HTML code to the server to be cleaned up
 */
function _sprawk_filter_word_process($text, $filter, $format)
{

  $key = 'sprawk:word_filter:' . md5($text);

  // check the cache first
  // we need to check that the orig data in the cached object is actually the same (there is a small chance that two orig strings with the same md5 crop up from time to time)
  if (($cache = cache_get($key)) && !empty ($cache->data)) {
    $data = $cache->data;
    if (strcmp($data["orig"], $text) == 0 && $data["output"] != '') {
      return $data["output"];
    } else {
      watchdog('sprawk', "Word filter cache for key %key contains different orig: %orig", array('%key' => $key, '%orig' => $data["orig"]));
    }
  }

  // prepare the request, add a parameter to prevent
  // recursively attempting to translate
  $uri = '/api/mswordCleaner.jsp';

  // Work out the data
  $params = array(
    "t" => $text
  );

  $result = _sprawk_doapi($uri, $params);
  $result["orig"] = $text;

  if ($result['status'] == 'success' && $result["output"] != '') {
    //Save item to cache
    $days = variable_get('sprawk_cache_lifetime', 30);
    cache_set($key, $result, 'cache_sprawk', time() + ($days * 24 * 60 * 60));
    return $result["output"];
  } else {

    // the API call failed
    return $text;
  }

}

function _sprawk_doapi($uri, $params = array())
{

  global $base_url;

  // which sprawk host to use
  $host = variable_get('sprawk_server', 'www.sprawk.com'); // beta.sprawk.com is the development server

  $params["g"] = variable_get('sprawk_groupcode', ''); // Sprawk group code
  $params["k"] = variable_get('sprawk_grouppassword', ''); // Sprawk team key
  $params["chk"] = SPRAWK_CHK; // $sprawk_chk; "\xE5\xE4\xF6"; // utf8_encode("\xE5\xE4\xF6"); // encoding check
  $params["src"] = $base_url . '/' . $_GET['q']; // request_uri(); // send the url of the page on this site to sprawk

  // Note that encoding seems to be done now automatically in the POST (in Drupal 6.x at least)
  // so no need to manually urlecode while building the reqcontent.

  $reqcontent = http_build_query($params, '', '&');
  if (variable_get('sprawk_debug', 0)) {
    watchdog('sprawk', "reqcontent=!rc", array('!rc' => $reqcontent), WATCHDOG_DEBUG);
  }

  $content = ""; // store result here

  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
    'Accept-Charset' => 'utf-8'
  );

  $url = 'http://' . $host . $uri;
  $result = drupal_http_request($url, array('headers' => $headers, 'method' => 'POST', 'data' => $reqcontent));

  if (variable_get('sprawk_stats_enabled', '0')) {
    sprawk_save_log($uri, $params, $result->code);
  }

  // Process HTTP response code.
  switch ($result->code) {
    case 400:
      // bad request
      watchdog('sprawk', 'Bad request %url; from page: %page',
        array('%page' => request_uri(), '%url' => $url));
      break;

    case 200:
    case 302:
    case 307:
      // Success
      $content = $result->data;
      // check weird start
      if (strpos($content, "1ff8") !== FALSE) {
        watchdog('sprawk', "Result contained 1ff8!!!");
        $content = str_replace("1ff8", "", $content);
      }
      // check the data we got:
      if (!strlen($content)) {
        watchdog('sprawk', 'Attempt failed calling API %api, from %url, no content from Sprawk server (HTTP code was %httpReturn)',
          array('%api' => $url, '%url' => request_uri(), '%httpReturn' => $result->code));
        return array("status" => 'failed');
      } else {
        if (strlen($content) > 0) {

          $result = array(
            "status" => "success",
            "output" => $content
          );
          return $result;

        } else {
          watchdog('sprawk', 'Attempt failed to clean HTML from %url; content I got was: %content',
            array('%url' => request_uri(), '%content' => $content));
          return array("status" => 'failed');
        }
      }

      break;

    default:
      watchdog('sprawk', 'Error processing request to %url with params %params from page: %page, due to result code %code and error %error.',
        array('%url' => $url, '%params' => print_r($params, TRUE), '%page' => request_uri(), '%error' => $result->error, '%code' => $result->code));
  }

  return array("status" => 'other');
}

/**
 * function _sprawk_match_tags().
 * (filter $tags containing $string)
 */
function _sprawk_match_tags($string, $tags)
{
  $matches = array();
  foreach ($tags as $tag) {
    if (!(strpos($tag, $string) === FALSE)) {
      $matches[$tag] = check_plain($tag);
    }
  }

  return $matches;
}

function sprawk_ct($string, $args = 0, $options = array())
{
  return ct($string, $args, $options);
}

/**
 * Content translation
 *
 * @param $string
 * @param $args
 * @return string
 */
function ct($string, $args = array(), $options = array())
{
  $topic = (isset($options['topic']) ? $options['topic'] : '');
  $contentType = (isset($options['contentType']) ? $options['contentType'] : '');
  global $language;
  // Merge in default.
  if (empty($options['langcode'])) {
    $options['langcode'] = isset($language->language) ? $language->language : 'en';
  }
  if (function_exists('locale') && $options['langcode'] != 'en') {
    $string = _sprawk_filter_translate($string, $options['langcode'], $topic, $contentType);
  }
  if (!$args) {
    return $string;
  } else {
    // Transform arguments before inserting them
    foreach ($args as $key => $value) {
      switch ($key[0]) {
        // Escaped only
        case '@':
          $args[$key] = check_plain($value);
          break;

        // Escaped and placeholder
        case '%':
        default:
          $args[$key] = theme('placeholder', $value);
          break;
        // Pass-through
        case '!':
      }
    }
    return strtr($string, $args);
  }
}

/**
 * Tests if a text starts with an given string.
 *
 * @param     string
 * @param     string
 * @return    bool
 */
function _sprawk_startsWith($Haystack, $Needle)
{
  // Recommended version, using strpos
  return strpos($Haystack, $Needle) === 0;
}

function _sprawk_div($in)
{
  return "<div>" . $in . "</div>\n";
}


/**
 * Get a list of topics for the team
 */
function _sprawk_fetch_topics()
{

  $result = _sprawk_doapi('/api/getTopics');

  if ($result['status'] == 'success' && $result["output"] != '') {
    if (variable_get('sprawk_debug', 0)) {
      watchdog("sprawk", "result was !res", array('!res' => $result["output"]), WATCHDOG_DEBUG);
    }

    $json = json_decode($result["output"], TRUE);
    if (variable_get('sprawk_debug', 0)) {
      watchdog("sprawk", "getTopics json: !json", array('!json' => var_export($json, TRUE)), WATCHDOG_DEBUG);
    }
    return $json;

  } else {
    watchdog("sprawk", "Call to API failed: !res", array('!res' => var_export($result, TRUE)), WATCHDOG_DEBUG);
    // the API call failed
    return array();
  }

}

/**
 * Cleanup 'smart quotes' to the easier-to-handle single quote
 */
function _sprawk_webclean($in)
{
  $in = str_replace(_sprawk_unicode2utf8("2018"), "'", $in);
  $in = str_replace(_sprawk_unicode2utf8("2019"), "'", $in);
  $in = str_replace(_sprawk_unicode2utf8("02BB"), "'", $in);
  $in = str_replace(_sprawk_unicode2utf8("02BC"), "'", $in);
  return $in;
}

/**
 *
 * @param $code string Unicode hex like in Java "2018" is a left curly quote
 * @return string
 */
function _sprawk_unicode2utf8($code)
{
  $c = hexdec($code);

  if ($c < 0x80) {
    return chr($c);
  } elseif ($c < 0x800) {
    return chr(0xc0 | ($c >> 6)) . chr(0x80 | ($c & 0x3f));
  } elseif ($c < 0x10000) {
    return chr(0xe0 | ($c >> 12)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
  } elseif ($c < 0x200000) {
    return chr(0xf0 | ($c >> 18)) . chr(0x80 | (($c >> 12) & 0x3f)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
  }
  return FALSE;
}

/**
 * Implements hook_flush_caches().
 */
function sprawk_flush_caches()
{
  // Request a flush of our cache table.
  return array('cache_sprawk');
}

/**
 * Clears cache_sprawk table
 */
function sprawk_clear_cache_submit($form, &$form_state)
{
  cache_clear_all('*', 'cache_sprawk', TRUE);
  drupal_set_message(t('Sprawk cache successfully reset'));
}

/**
 *
 * Save translation setting for node type
 */
function sprawk_translation_form_submit($form, &$form_state)
{
  if ($form['#node_type']->type) variable_set('sprawk_ct_' . $form['#node_type']->type, $form_state['values']['sprawk_setting']);
}

/**
 *
 * Save a log with API call information
 *
 * @param     $arams: array with translation information
 * @param     code: integer code for translation result
 */
function sprawk_save_log($uri, $params, $code)
{
  db_query("INSERT INTO {sprawk_log} VALUES (NOW(), '%s', '%s', %d, '%s', '%s')",
    $uri, $params['tl'], $code, $params['src'], $params['t']);
}

/**
 * Returns the (last known) status of the configured Sprawk API keys.
 *
 * @param $reset
 *   (optional) Boolean whether to reset the stored state and re-check.
 *   Defaults to FALSE.
 *
 * @return
 *   TRUE if the module is considered operable, or an associative array
 *   describing the current status of the module:
 *   - keys: Boolean whether Sprawk API keys have been configured.
 *   - keys valid: TRUE if Sprawk API keys are valid, or the error code as
 *     returned by Sprawk servers.
 *   - servers: Boolean whether there is a non-empty list of Sprawk servers.
 *
 */
function _sprawk_status($reset = FALSE)
{
  // Load stored status.
  $status = variable_get('sprawk_status', array(
    'keys' => FALSE,
    'keys valid' => FALSE,
  ));

  // Both API keys are required.
  $groupcode = variable_get('sprawk_groupcode', '');
  $grouppassword = variable_get('sprawk_grouppassword', '');
  $status['keys'] = (!empty($groupcode) && !empty($grouppassword));

  // If we have keys and are asked to reset, check whether keys are valid.
  if ($status['keys'] && $reset) {
    $status['keys valid'] = _sprawk_checkpass($groupcode, $grouppassword);
  } // Otherwise, if there are no keys, they cannot be valid.
  elseif (!$status['keys']) {
    $status['keys valid'] = FALSE;
  }

  // In case of an error, indicate whether we have a non-empty server list.
  if ($status['keys valid'] !== TRUE) {
    $server = variable_get('sprawk_server', '');
    $status['server'] = !empty($server);
  }

  // Update stored status upon reset.
  if ($reset) {
    variable_set('sprawk_status', $status);
  }

  return ($status['keys valid'] === TRUE ? TRUE : $status);
}

function _sprawk_checkpass($groupcode, $grouppassword)
{

  $params["groupCode"] = $groupcode;
  $params["password"] = $grouppassword;
  $params["mode"] = 'binary';

  $reqcontent = http_build_query($params, '', '&');

  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
    'Accept-Charset' => 'utf-8'
  );

  $url = 'http://beta.sprawk.com/api/testGroupPass.jsp';
  $result = drupal_http_request($url, array(
    'headers' => $headers,
    'method' => 'POST',
    'data' => $reqcontent));

  $content = $result->data;
  return (!empty($content) && trim($content) == '1');

}

/**
 * Implement for D7 (like D6 hook_footer) to inject the sprawk group key and hash into each page so that javascript translations can authenticate correctly
 */
function sprawk_page_alter(&$page)
{
  $footer = "<script type='text/javascript'>" .
      "var sprawk_g='" . variable_get('sprawk_groupcode', '') .
      "';var sprawk_k='" . variable_get('sprawk_grouppassword', '') . "';</script>";
  $linkMode = variable_get('sprawk_linkmode', 'PREFIX_DRUPAL'); // Whether sprawk will adjust links
  $server = variable_get('sprawk_server', 'www.sprawk.com');
  $su = $_SERVER['SERVER_NAME']; // gethostname();
  $groupcode = variable_get('sprawk_groupcode', '');
  $footer .= '<script type="text/javascript" src="http://' . $server .
      '/switcher.js?g=' . $groupcode . '&amp;su=' . $su . '&amp;mode=' . $linkMode . '"></script>';
  $page['page_bottom']['sprawk_scripts'] = array(
    '#markup' => $footer,
    '#weight' => 25
  );
}

function sprawk_instructions($linkMode, $sprawkServer = NULL)
{
  if (!$sprawkServer)
    $sprawkServer = variable_get('sprawk_server', 'www.sprawk.com');

  // Work out the data
  $params = array(
    "su" => $_SERVER['SERVER_NAME'],
    "software" => $_SERVER['SERVER_SOFTWARE'],
    "server" => $sprawkServer,
    "mode" => $linkMode,
  );

  $result = _sprawk_doapi('/api/configInstructions', $params);

  if ($result['status'] == 'success') {
    return $result["output"];
  } else {
    watchdog('sprawk', "Error loading config instructions: " . $result['status']);
  }

  return '';
}

function sprawk_updateDescLinkmode($form, $form_state)
{
  // Get field name
  // $fieldname = $form_state['triggering_element']['#name'];
  // dpr($form_state['values']);
  $linkMode = $form_state['values']['sprawk_linkmode'];
  // dpr($linkMode);
  return _sprawk_linkMode_desc($linkMode);
}

function _sprawk_linkMode_desc($linkMode)
{
  // error_log('linkMode=' . $linkMode);
  if ($linkMode == 'PREFIX_DRUPAL') {
    $inner = 'This option requires a ProxyPass setting in your webserver configuration.';
  } else if ($linkMode == 'PROXY') {
    $inner = 'This option requires a configuration in your domain\'s DNS settings.';
  } else {
    $inner = 'This link mode is no longer supported';
  }

  return '<div id="sprawk_linkmode_desc">' . $inner . '</div>';

}
