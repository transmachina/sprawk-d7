var sprawk = {};

(function ($) {

  sprawk.poptopics = function (selectId, currVal) {
    var code = $("#edit-sprawk-groupcode").val();
    var pass = $("#edit-sprawk-grouppassword").val();
    $.getJSON("/sprawk/api/groupTopicsJSON.jsp", {groupCode: code, password: pass}, function (j) {
      var options = '';
      options += '<option value=\'\'>' + 'Select a topic' + '</option>';
      for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].optionValue + '" ' + (currVal == j[i].optionValue ? 'selected="selected"' : "") + '>' + j[i].optionDisplay + '</option>';
      }
      $("select#" + selectId).html(options);
    });
  };

  sprawk.testpass = function () {
    var code = $("#edit-sprawk-groupcode").val();
    var pass = $("#edit-sprawk-grouppassword").val();
    $('#sprawk-testpassresult').load('/sprawk/api/testGroupPass.jsp?groupCode=' + code + '&password=' + encodeURIComponent(pass));
  };

  /**
   *
   * @param sourceText
   */
  sprawk.t = function (sourceText) {
    var retVal = sourceText;
    if (userLocale == 'en') {
      // no need to translate, but we need to build the string if it's parameterized
      /* This will only work well as long as there are no gaps in the sequence of parameters and the sequence
       begins at 0. I.e., 'my text {0} {2}' won't work as there is no {1} */
      for (var i = 1; i < arguments.length; i++) {
        retVal = retVal.replace(new RegExp('\{' + (i - 1) + '\}', 'g'), arguments[i]);
      }
      retVal = this.applyWikiFormatting(retVal);
    }
    else if (sourceText !== undefined && $.trim(sourceText).length !== 0) {
      // iterate through params
      var url = '/sprawk/api/translateHtmlSnippet';
      var params = 't=' + encodeURIComponent(sourceText) + '&tl=' + userLocale + '&g=' + sprawk_g + '&k=' + sprawk_k + '&src=' + encodeURIComponent(window.location.href);
      for (var i = 1; i < arguments.length; i++) {
        params += '&param=' + encodeURIComponent(arguments[i]);
      }
      jQuery.ajax({
        async: false,
        type: 'get',
        url: url,
        data: params,
        success: function (data, textStatus, XMLHttpRequest) {
          retVal = data;
        }
      });
    }
    return retVal;
  };

  /* This function returns the original source, assembled from parameters if needed,
   * and invokes the callback function with the result of the translation. */
  sprawk.aT = function (sourceText, callback) {
    var processed = sourceText;
    /* Apply pattern and parameters */
    for (var i = 2; i < arguments.length; i++) {
      processed = processed.replace(new RegExp('\{' + (i - 2) + '\}', 'g'), arguments[i]);
    }

    processed = sprawk.applyWikiFormatting(processed);

    if (userLocale != 'en') {
      if (sourceText !== undefined && $.trim(sourceText).length !== 0) {
        // iterate through params
        var url = '/sprawk/api/translateHtmlSnippet';
        var params = 't=' + encodeURIComponent(sourceText) + '&tl=' + userLocale + '&g=' + sprawk_g + '&k=' + sprawk_k + '&src=' + encodeURIComponent(window.location.href);
        for (var i = 2; i < arguments.length; i++) {
          params += '&param=' + encodeURIComponent(arguments[i]);
        }
        jQuery.ajax({
          async: true,
          type: 'get',
          url: url,
          data: params,
          success: function (data) {
            callback(data);
          }
        });
      }
    }
    return processed;
  };

  sprawk.applyWikiFormatting = function (text) {
    text = text.replace(/\*([^<>]*?)\*/g, '<b>$1</b>'); //Bold text
    text = text.replace(/#([^<>]*?)#/g, '<em>$1</em>'); //Emphasized text
    text = text.replace(/\/([^<>]*?)\//g, '<i>$1</i>'); //Italic text
    text = text.replace(/\[(sprawk)\]/i, '<span class="lowlight">$1</span>'); //Style the sprawk "logo"
    return text;
  };

  /* This function takes a string in the form of a variable name (including "complex"
   * like myObj.property). The variable "root" must be global but the window scope should
   * not be explicit. Second parameter is the string to be translated. Third argument is
   * an optional callback function to be invoked when the translation is available. Any additional
   * arguments are passed as translation parameters to aT() */
  sprawk.assignAT = function (identifier, value, callbackOrFirstParam) {
    /* Force dot notation for easy splitting, if not in that format already */
    identifier = identifier.replace(/^\s*\[(.*?)\]\s*$/, '$1'); //Remove surrounding brackets
    identifier = identifier.replace(/\]\[/, '.'); //Replace "delimiting" brackets with dots
    var parts = identifier.split('.');

    /* If no value is passed, use the value already in the named variable */
    if (typeof value == 'undefined' || value === null || value === '') {
      value = this.getVar(parts);
    }

    /* Check to see if third argument exists and if so if it's a function or a translation param */
    var firstParam = 2;
    var extraCallback = function () {
    };
    if (typeof callbackOrFirstParam == 'function') {
      extraCallback = callbackOrFirstParam;
      firstParam = 3;
    }

    /* Orignally we used the static Array.concat() but that was not widely supported yet */
    var args = [];
    args.push(value);
    args.push(function (text) {
      sprawk.assignVar(parts, text);
      extraCallback(text);
    });
    args.concat(this.asArray(arguments).slice(firstParam));
    /* Returns the value that is also assigned to the
     * variable named in identifier (parts). The value is the un-translated but parameter-parsed
     * string that aT() returns. aT() is provided a callback function that assigns the translated
     * value to the variable named in identifier once returned from server.
     */
    return this.assignVar(parts, this.aT.apply(null, args));
  };

  sprawk.getVar = function (parts) {
    var cur = window;
    for (var i = 0; i < parts.length; i++) {
      cur = cur[parts[i]];
    }
    return cur;
  };

  sprawk.assignVar = function (parts, value) {
    var cur = window;
    for (var i = 0; i < parts.length - 1; i++) {
      cur = cur[parts[i]];
    }
    cur[parts[i]] = value;
    return value; //Not needed but useful
  };

  sprawk.asArray = function (args) {
    var arr = [];
    for (var i = 0; i < args.length; i++) {
      arr[i] = args[i];
    }
    return arr;
  };

  jQuery.fn.aTHtml = function (text) {
    var me = this;
    /* Orignally we used the static Array.concat() but that was not widely supported yet */
    var args = [];
    args.push(text);
    args.push(function (translated) {
      me.html(translated);
    });
    args.concat(asArray(arguments).slice(1));
    me.html(sprawk.aT.apply(null, args));
    return this;
  };

  Drupal.behaviors.sprawk_test_pass = function (context) {
    $('.form-test-button').bind('click', function () {
      sprawk.testpass();
      return false;
    });
  }

})(jQuery);
